# SimpleGL

A simple and fast way to play with shaders in the browser.

## Usage

1. Launch index.html from your local server.
(Due to security restrictions you cannot open it from the disk.)

This sample uses SimpleGLImage to apply `#source` as texture onto a
quad. This means that by default you should see the source as is, until
you start to play with the shader.

2. Start modifying the shader code and play !

## Modifying the shader

The vertex and fragment shaders are included in line in index.html,
you can start coding directly inside
`line 18`
```
<script id="fs" type="application/x-glsl-shader" >
...
</script>
```

## Modifying the source

This sample will load any drawable node with id `#source`,
this can be a `image` node, a `video` node or an other `canvas`

## Adding variables

SimpleGL uses a simple interface for declaring your variables,
you only need to pass them with the correct namings when calling
`draw(...)`.

Check out the `render` function in the sample
```
function render(dt) {

    if(!sourceTex)
        sourceTex = gl.createTexture();
    gl.updateTexture(sourceTex, source);

    gl.draw({
        'texture_uSampler': sourceTex,
        'float_dt': dt
    });

}
```

`draw()` takes an object wherein in key is formated `[type]_[name]`,
depending on `type` the correct `uniform` will be decalared and passed
on to the shader.

types include

| SimpleGL type | GLSL type | value type |
|---|---|---|
| bool  | bool  | interpreted as bool |
| int  | int  | number  |
| ivec2  | ivec2  | array of 2 numbers  |
| ivec3  | ivec3  | array of 3 numbers  |
| ivec4  | ivec4  | array of 4 numbers  |
| float  | float  | number  |
| vec2  | vec2  | array of 2 numbers  |
| vec3  | vec3  | array of 3 numbers  |
| vec4  | vec4  | array of 4 numbers  |
| mat2  | mat2  | array of 4 numbers  |
| mat3  | mat3  | array of 9 numbers  |
| mat4  | mat4  | array of 16 numbers  |
| texture  | sampler2D  | GL_Texture2D  |
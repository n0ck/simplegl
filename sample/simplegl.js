function SimpleGL(canvas) {
    this.canvas = canvas;
    var gl = this.gl = canvas.getContext('webgl') || canvas.getContext("experimental-webgl");
    this.drawMode = this.gl.TRIANGLE_STRIP;
}
SimpleGL.prototype.constructor = SimpleGL;
SimpleGL.prototype.setProgram =
    function(shaderProgram) {
        this.shaderProgram = shaderProgram;
        this.gl.useProgram(this.shaderProgram);

        this.shaderProgram.vertexPositionAttribute = this.gl.getAttribLocation(this.shaderProgram, "aVertexPosition");
        this.gl.enableVertexAttribArray(this.shaderProgram.vertexPositionAttribute);
    };

SimpleGL.prototype.createProgram =
    function(vs, fs) {
        
        function getFragmentShader(gl, src) {
            var shader = gl.createShader(gl.FRAGMENT_SHADER);
            gl.shaderSource(shader, src);
            gl.compileShader(shader);
            if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
                console.log(gl.getShaderInfoLog(shader));
                return null;
            }
            return shader;
        }
        
        function getVertexShader(gl, src) {
            var shader = gl.createShader(gl.VERTEX_SHADER);
            gl.shaderSource(shader, src);
            gl.compileShader(shader);
            if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
                console.log(gl.getShaderInfoLog(shader));
                return null;
            }
            return shader;
        }
    
        var vertexShader = getVertexShader(this.gl, vs);
        var fragmentShader = getFragmentShader(this.gl, fs);

        var shaderProgram = this.gl.createProgram();
        this.gl.attachShader(shaderProgram, vertexShader);
        this.gl.attachShader(shaderProgram, fragmentShader);
        this.gl.linkProgram(shaderProgram);
        if (!this.gl.getProgramParameter(shaderProgram, this.gl.LINK_STATUS)) {
            throw "Could not initialise shaders";
        }
    
        return shaderProgram;
    };


SimpleGL.prototype.setVertices =
    function( vertices, itemSize ){
    
        itemSize = itemSize || 3;
        var numItems = Math.floor(vertices.length/itemSize);
        
        this.vertices = new Float32Array(vertices);
        this.vertexPositionBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexPositionBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, this.vertices, this.gl.STATIC_DRAW);
        this.vertexPositionBuffer.itemSize = itemSize;
        this.vertexPositionBuffer.numItems = numItems;
        
    };
        
SimpleGL.prototype.draw =
    function(params) {
        
        var width = this.gl.canvas.clientWidth;
        var height = this.gl.canvas.clientHeight;
        if (this.gl.canvas.width != width || this.gl.canvas.height != height) {
            this.gl.canvas.width = width;
            this.gl.canvas.height = height;
            this.gl.viewport(0, 0, this.gl.drawingBufferWidth, this.gl.drawingBufferHeight);
        }
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexPositionBuffer);
        this.gl.vertexAttribPointer(
            this.shaderProgram.vertexPositionAttribute,
            this.vertexPositionBuffer.itemSize,
            this.gl.FLOAT, 
            false, 0, 0);

        if (params)
            for( var p in params) {
                this.gl.uniform1f(this.gl.getUniformLocation(this.shaderProgram, p), params[p] );
            }
        
        this.gl.drawArrays(this.drawMode, 0, this.vertexPositionBuffer.numItems);
    
    };

function SimpleGLImage(canvas) {
    SimpleGL.apply(this, arguments);
    this.textures = [];
}
SimpleGLImage.prototype = Object.create(SimpleGL.prototype)
SimpleGLImage.prototype.constructor = SimpleGLImage;
SimpleGLImage.prototype.setProgram =
    function(shaderProgram) {
        SimpleGL.prototype.setProgram.apply(this, arguments);

        this.shaderProgram.textureCoordAttribute = this.gl.getAttribLocation(this.shaderProgram, "aTextureCoord");
        this.gl.enableVertexAttribArray(this.shaderProgram.textureCoordAttribute);
    };

SimpleGLImage.prototype.setVertices =
    function( vertices, itemSize ){
    
        itemSize = itemSize || 3;
        var numItems = Math.floor(vertices.length/itemSize);
        
        this.vertices = new Float32Array(vertices);
        this.vertexPositionBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexPositionBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, this.vertices, this.gl.STATIC_DRAW);
        this.vertexPositionBuffer.itemSize = itemSize;
        this.vertexPositionBuffer.numItems = numItems;
        
    };
SimpleGLImage.prototype.setTextureCoordinates =
    function( textureCoordinates ){
        
        this.verticesTextureCoordBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.verticesTextureCoordBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(textureCoordinates), this.gl.STATIC_DRAW);
        this.verticesTextureCoordBuffer.itemSize = 2;
        this.verticesTextureCoordBuffer.numItems = textureCoordinates.length/2;
    
    };

SimpleGLImage.prototype.setVerticesForRatio =
    function( srcToDestWidthRatio, srcToDestHeightRatio ){
        
        var ruleSize = Math.max(srcToDestWidthRatio, srcToDestHeightRatio);
        srcToDestWidthRatio /= ruleSize;
        srcToDestHeightRatio /= ruleSize;
        this.vertices = new Float32Array([
            -srcToDestWidthRatio, -srcToDestHeightRatio, 0,
            -srcToDestWidthRatio, srcToDestHeightRatio, 0,
            srcToDestWidthRatio, -srcToDestHeightRatio, 0,
            srcToDestWidthRatio, srcToDestHeightRatio, 0
        ]);
        
        this.vertexPositionBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexPositionBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, this.vertices, this.gl.STATIC_DRAW);
        this.vertexPositionBuffer.itemSize = 3;
        this.vertexPositionBuffer.numItems = 4;

        this.verticesTextureCoordBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.verticesTextureCoordBuffer);
        var textureCoordinates = [
            0.0,  0.0,
            0.0,  1.0,
            1.0,  0.0,
            1.0,  1.0
        ];
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(textureCoordinates), this.gl.STATIC_DRAW);
        this.verticesTextureCoordBuffer.itemSize = 2;
        this.verticesTextureCoordBuffer.numItems = 4;
        
    };

SimpleGLImage.prototype.createTexture =
    function() {
        tex = this.gl.createTexture();
        this.gl.bindTexture(this.gl.TEXTURE_2D, tex);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
        return tex;
    };
        
SimpleGLImage.prototype.updateTexture =
    function(tex, src) {
        
        if(!tex) return
        if( (src.videoHeight === undefined && src.height < 1) || src.videoHeight < 1 ) {
            console.log('invalid source provided to updateTexture');
            return;
        }
        
        this.gl.bindTexture(this.gl.TEXTURE_2D, tex);
        this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, true);
        this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, src);

    };
        
SimpleGLImage.prototype.draw = 
    function(params, overrideWidth, overrideHeight) {
        
        var width = overrideWidth || this.gl.canvas.clientWidth;
        var height = overrideHeight || this.gl.canvas.clientHeight;
        if (this.gl.canvas.width != width || this.gl.canvas.height != height) {
            this.gl.canvas.width = width;
            this.gl.canvas.height = height;
            this.gl.viewport(0, 0, this.gl.drawingBufferWidth, this.gl.drawingBufferHeight);
        }
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexPositionBuffer);
        this.gl.vertexAttribPointer(
            this.shaderProgram.vertexPositionAttribute,
            this.vertexPositionBuffer.itemSize,
            this.gl.FLOAT, 
            false, 0, 0);

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.verticesTextureCoordBuffer);
        this.gl.vertexAttribPointer(
            this.shaderProgram.textureCoordAttribute, 
            this.verticesTextureCoordBuffer.itemSize, 
            this.gl.FLOAT,
            false, 0, 0);
            
        
        if (params) {
            var texIndex = 1;
            for( var p in params) {
                var pInfo = p.split('_');
                if(pInfo.length != 2) throw "Invalid uniform " + p + " in draw call, please specify [type]_[uniform name]";
                var pType = pInfo[0];
                var pName = pInfo[1];
                var pValue = params[p];

                var uLocation = this.gl.getUniformLocation(this.shaderProgram, pName);

                switch(pType){
                    case 'bool':    this.gl.uniform1i( uLocation, pValue ? 1 : 0 ); break;
                    case 'int':     this.gl.uniform1i( uLocation, pValue ); break;
                    case 'ivec2':   this.gl.uniform2i( uLocation, pValue[0], pValue[1] ); break;
                    case 'ivec3':   this.gl.uniform3i( uLocation, pValue[0], pValue[1], pValue[2] ); break;
                    case 'ivec4':   this.gl.uniform4i( uLocation, pValue[0], pValue[1], pValue[2], pValue[3] ); break;
                    case 'float':   this.gl.uniform1f( uLocation, pValue ); break;
                    case 'vec2':    this.gl.uniform2f( uLocation, pValue[0], pValue[1] ); break;
                    case 'vec3':    this.gl.uniform3f( uLocation, pValue[0], pValue[1], pValue[2] ); break;
                    case 'vec4':    this.gl.uniform4f( uLocation, pValue[0], pValue[1], pValue[2], pValue[3] ); break;
                    case 'int[]':   this.gl.uniform1iv( uLocation, pValue ); break;
                    case 'ivec2[]': this.gl.uniform2iv( uLocation, pValue ); break;  
                    case 'ivec3[]': this.gl.uniform3iv( uLocation, pValue ); break;  
                    case 'ivec4[]': this.gl.uniform4iv( uLocation, pValue ); break;  
                    case 'float[]': this.gl.uniform1fv( uLocation, pValue ); break;
                    case 'vec2[]':  this.gl.uniform2fv( uLocation, pValue ); break; 
                    case 'vec3[]':  this.gl.uniform3fv( uLocation, pValue ); break;
                    case 'vec4[]':  this.gl.uniform4fv( uLocation, pValue ); break;
                    case 'mat2':    this.gl.uniformMatrix2fv( uLocation, false, pValue ); break;
                    case 'mat3':    this.gl.uniformMatrix3fv( uLocation, false, pValue ); break;
                    case 'mat4':    this.gl.uniformMatrix4fv( uLocation, false, pValue ); break;
                        
                    case 'buffer1', 'buffer1', 'buffer3', 'buffer4': 
                        var buf = this.gl.createBuffer();
                        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buf);
                        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(pValue), this.gl.STATIC_DRAW);
                        this.gl.vertexAttribPointer(
                            uLocation,
                            parseInt(pType.slice(-1)),
                            this.gl.FLOAT,
                            false, 0, 0);
                        break;
                    
                    case 'texture':
                        this.gl.activeTexture(this.gl['TEXTURE'+texIndex]);
                        this.gl.bindTexture(this.gl.TEXTURE_2D, pValue);
                        this.gl.uniform1i(uLocation, texIndex);
                        ++texIndex;
                        break;
                    
                    default: throw "Unknown type "+pType+" for uniform " + p + " in draw call";
                }
            }
        }
        
       this.gl.drawArrays(this.gl.TRIANGLE_STRIP, 0, this.vertexPositionBuffer.numItems);
        
    };

SimpleGLImage.prototype.drawBuffer =
    function(bufferName, width, height, params, textureParams){
        this.frameBuffer = this.frameBuffer || {};
        var bufData = this.frameBuffer[bufferName];
        if(bufData == null ){
            bufData = this.frameBuffer[bufferName] = {};
            bufData.width = width;
            bufData.height = height;
            bufData.buffer = this.gl.createFramebuffer();
            this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, bufData.buffer);
            
            bufData.texture = this.gl.createTexture();
            this.gl.bindTexture(this.gl.TEXTURE_2D, bufData.texture);
            var filter = (textureParams && textureParams.filter == 'nearest') ? this.gl.NEAREST : this.gl.LINEAR;
            this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, filter);
            this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, filter);
            this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
            this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
            
            this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, width, height, 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, null);
        } else {
            if(width != bufData.width) {
                this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, width, height, 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, null);
            }
            
            this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, bufData.buffer);
        }
        
        this.gl.viewport(0, 0, width, height);
    
        this.vertexPositionBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexPositionBuffer);
        if(!this.fullScreenVertices) this.fullScreenVertices = new Float32Array([-1,-1,0,-1,1,0,1,-1,0,1,1,0]);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, this.fullScreenVertices, this.gl.STATIC_DRAW);
        this.vertexPositionBuffer.itemSize = 3;
        this.vertexPositionBuffer.numItems = 4;
    
        this.gl.framebufferTexture2D(this.gl.FRAMEBUFFER, this.gl.COLOR_ATTACHMENT0, this.gl.TEXTURE_2D, bufData.texture, 0);
    
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.frameBuffer[bufferName].buffer);
        this.draw(params);
    
        if(textureParams && textureParams.outputArray) {
            this.gl.readPixels(0, 0, width, height, this.gl.RGBA, this.gl.UNSIGNED_BYTE, textureParams.outputArray)
        }
    
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
    
        this.vertexPositionBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexPositionBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, this.vertices, this.gl.STATIC_DRAW);
        this.vertexPositionBuffer.itemSize = 3;
        this.vertexPositionBuffer.numItems = 4;
    
        this.gl.viewport(0, 0, this.gl.drawingBufferWidth, this.gl.drawingBufferHeight);
    
        return bufData.texture;
    };
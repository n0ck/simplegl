window.saveload = {
    log: false,
    init: function(className) {
        document.querySelectorAll('.'+className).forEach(function(el){
            el.addEventListener('change', window.saveload.save.bind(null, className));
        });
        this.load(className);
    },

    save: function(className) {
        var params = Array.prototype.slice.apply(document.querySelectorAll('.'+className)).reduce(function(sum, el){
                if(this.log) console.log(sum, el);
                sum[el.id] = el.value;
                return sum;
            }, {});
            document.cookie = className+'='+JSON.stringify(params);
        if(this.log) console.log('saved', params);
    },

    getCookie: function(name){
        if(this.log) console.log(document.cookie);
        return document.cookie.split(';').filter(function(cookie){
            while(cookie.charAt(0) == ' ') cookie = cookie.substr(1);
            if(this.log) console.log(cookie);
            return cookie.substr(0, name.length+1) == (name+'=')
        }).reduce(function(sum,e){
            while(e.charAt(0) == ' ') e = e.substr(1);
            return sum + e.substr(name.length+1);
        }, '');
    },
    
    load: function(className) {
        var cookie = this.getCookie(className);
        if(this.log) console.log('loading', cookie);
        var params;
        try{
            params = JSON.parse(cookie);
        }catch(e){}
        if(params){
            if(this.log) console.log(params);
            document.querySelectorAll('.'+className).forEach(function(el){
                el.value = params[el.id];
                if(this.log) console.log(el.id, params[el.id], el.value);
            });
        }

    }
};
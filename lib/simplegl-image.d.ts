import SimpleGL from './simplegl';
export default class SimpleGLImage extends SimpleGL {
    protected textures: WebGLTexture[];
    protected additionalShaderAttributes: {
        textureCoordinate?: number;
    };
    protected textureCoordinates: {
        data: Float32Array;
        buffer: WebGLBuffer;
        itemSize: number;
        numItems: number;
    };
    constructor(canvas: HTMLCanvasElement);
    setProgram(shaderProgram: WebGLProgram): void;
}

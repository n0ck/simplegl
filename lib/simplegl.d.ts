import ISGLFrameBuffer from "./ISGLFrameBuffer";
export type FrameBuffer = ISGLFrameBuffer;
export interface SimpleGLOptions extends WebGLContextAttributes {
}
export default class SimpleGL {
    protected canvas: HTMLCanvasElement;
    protected context: WebGLRenderingContext;
    protected state: {
        framebuffer: WebGLFramebuffer | null;
        drawingWidth: number;
        drawingHeight: number;
    };
    protected shaderProgram?: WebGLProgram;
    protected shaderAttributes?: {
        vertexPosition: number;
    };
    protected vertices?: {
        data: Float32Array;
        buffer: WebGLBuffer;
        itemSize: number;
        numItems: number;
    };
    protected drawMode: number;
    constructor(canvas: HTMLCanvasElement, options?: SimpleGLOptions);
    get drawWidth(): number;
    get drawHeight(): number;
    getCanvas(): Readonly<HTMLCanvasElement>;
    getGLContext(): Readonly<WebGLRenderingContext>;
    setProgram(shaderProgram: WebGLProgram): void;
    createProgram(vs: string, fs: string): WebGLProgram;
    setVertices(vertices: number[], itemSize?: number): void;
    createTexture(): WebGLTexture;
    updateTexture(tex: WebGLTexture, src: TexImageSource): void;
    updateTexture(tex: WebGLTexture, width: number, height: number, data?: {
        format: number;
        type: number;
        pixels: ArrayBufferView;
    }): void;
    createFramebuffer(width?: number, height?: number): FrameBuffer;
    draw(params?: {
        [id: string]: boolean | number | number[] | WebGLTexture;
    }, framebuffer?: FrameBuffer): void;
    copyCurrentBufferToTexture(tx: WebGLTexture): void;
}

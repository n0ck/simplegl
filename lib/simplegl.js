"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var GL = WebGLRenderingContext;
var SimpleGL = /** @class */ (function () {
    function SimpleGL(canvas, options) {
        this.state = {
            drawingHeight: 0,
            drawingWidth: 0,
            framebuffer: null,
        };
        this.canvas = canvas;
        this.context = canvas.getContext('webgl', options);
        this.drawMode = GL.TRIANGLE_STRIP;
    }
    Object.defineProperty(SimpleGL.prototype, "drawWidth", {
        get: function () { return this.context.drawingBufferWidth; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SimpleGL.prototype, "drawHeight", {
        get: function () { return this.context.drawingBufferHeight; },
        enumerable: false,
        configurable: true
    });
    SimpleGL.prototype.getCanvas = function () { return this.canvas; };
    SimpleGL.prototype.getGLContext = function () { return this.context; };
    SimpleGL.prototype.setProgram = function (shaderProgram) {
        this.shaderProgram = shaderProgram;
        this.context.useProgram(this.shaderProgram);
        this.shaderAttributes = {
            vertexPosition: this.context.getAttribLocation(this.shaderProgram, 'aVertexPosition'),
        };
        this.context.enableVertexAttribArray(this.shaderAttributes.vertexPosition);
    };
    SimpleGL.prototype.createProgram = function (vs, fs) {
        function getFragmentShader(context, src) {
            var shader = context.createShader(GL.FRAGMENT_SHADER);
            context.shaderSource(shader, src);
            context.compileShader(shader);
            if (!context.getShaderParameter(shader, GL.COMPILE_STATUS)) {
                var err = context.getShaderInfoLog(shader);
                if (err) {
                    var match = /ERROR: (\d+):(\d+):/.exec(err);
                    if (match) {
                        var errIndex = match[0], char = match[1], line = match[2];
                        var errLine = src.split('\n')[parseInt(line, 10) - 1].trim();
                        var baseErr = err.split('\n');
                        err = [
                            baseErr[0].slice(errIndex.length).trim(),
                            "l.".concat(line, ": ").concat(errLine),
                            "".concat(new Array("l.".concat(line, ": ").length + 1 + parseInt(char, 10)).join(' '), "^"),
                            baseErr[1].slice(errIndex.length).trim()
                        ]
                            .concat(baseErr[2])
                            .join('\n');
                    }
                }
                throw new Error(err || 'unknown gl error');
            }
            return shader;
        }
        function getVertexShader(context, src) {
            var shader = context.createShader(GL.VERTEX_SHADER);
            context.shaderSource(shader, src);
            context.compileShader(shader);
            if (!context.getShaderParameter(shader, GL.COMPILE_STATUS)) {
                var err = context.getShaderInfoLog(shader);
                if (err) {
                    var match = /ERROR: (\d+):(\d+):/.exec(err);
                    if (match) {
                        var errIndex = match[0], char = match[1], line = match[2];
                        var errLine = src.split('\n')[parseInt(line, 10) - 1].trim();
                        var baseErr = err.split('\n');
                        err = [
                            baseErr[0].slice(errIndex.length).trim(),
                            "l.".concat(line, ": ").concat(errLine),
                            "".concat(new Array("l.".concat(line, ": ").length + 1 + parseInt(char, 10)).join(' '), "^"),
                            baseErr[1].slice(errIndex.length).trim()
                        ]
                            .concat(baseErr[2])
                            .join('\n');
                    }
                }
                throw new Error(err || 'unknown gl error');
            }
            return shader;
        }
        var vertexShader = getVertexShader(this.context, vs);
        var fragmentShader = getFragmentShader(this.context, fs);
        var shaderProgram = this.context.createProgram();
        if (shaderProgram == null) {
            throw new Error('error compiling program');
        }
        this.context.attachShader(shaderProgram, vertexShader);
        this.context.attachShader(shaderProgram, fragmentShader);
        this.context.linkProgram(shaderProgram);
        if (!this.context.getProgramParameter(shaderProgram, GL.LINK_STATUS)) {
            throw new Error('Could not initialise shaders');
        }
        return shaderProgram;
    };
    SimpleGL.prototype.setVertices = function (vertices, itemSize) {
        if (itemSize === void 0) { itemSize = 3; }
        var numItems = Math.floor(vertices.length / itemSize);
        var data = new Float32Array(vertices);
        var buffer = this.context.createBuffer();
        if (buffer == null) {
            throw new Error('error creating buffer');
        }
        this.context.bindBuffer(this.context.ARRAY_BUFFER, buffer);
        this.context.bufferData(this.context.ARRAY_BUFFER, data, GL.STATIC_DRAW);
        this.vertices = {
            buffer: buffer,
            data: data,
            itemSize: itemSize,
            numItems: numItems,
        };
    };
    SimpleGL.prototype.createTexture = function () {
        var tex = this.context.createTexture();
        this.context.bindTexture(GL.TEXTURE_2D, tex);
        this.context.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
        this.context.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);
        this.context.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE);
        this.context.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE);
        return tex;
    };
    SimpleGL.prototype.updateTexture = function (tex, srcOrWidth, height, data) {
        var _a, _b, _c;
        if (!tex) {
            return;
        }
        this.context.bindTexture(GL.TEXTURE_2D, tex);
        this.context.pixelStorei(GL.UNPACK_FLIP_Y_WEBGL, 1);
        if (typeof srcOrWidth === 'number') {
            var width = srcOrWidth;
            this.context.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, width, height, 0, (_a = data === null || data === void 0 ? void 0 : data.format) !== null && _a !== void 0 ? _a : GL.RGBA, (_b = data === null || data === void 0 ? void 0 : data.type) !== null && _b !== void 0 ? _b : GL.UNSIGNED_BYTE, (_c = data === null || data === void 0 ? void 0 : data.pixels) !== null && _c !== void 0 ? _c : null);
        }
        else {
            var src = srcOrWidth;
            if ((src instanceof HTMLVideoElement ? src.videoHeight : src.height) < 1) {
                throw new Error('invalid source provided to updateTexture');
            }
            this.context.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, GL.RGBA, GL.UNSIGNED_BYTE, src);
        }
    };
    SimpleGL.prototype.createFramebuffer = function (width, height) {
        var buf = this.context.createFramebuffer();
        this.context.bindFramebuffer(GL.FRAMEBUFFER, buf);
        var tex = this.createTexture();
        var w = width || this.canvas.width;
        var h = height || this.canvas.height;
        this.context.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, w, h, 0, GL.RGBA, GL.UNSIGNED_BYTE, null);
        this.context.framebufferTexture2D(GL.FRAMEBUFFER, GL.COLOR_ATTACHMENT0, GL.TEXTURE_2D, tex, 0);
        // clean
        this.context.bindFramebuffer(GL.FRAMEBUFFER, this.state.framebuffer);
        return {
            buffer: buf,
            height: h,
            texture: tex,
            width: w,
        };
    };
    SimpleGL.prototype.draw = function (params, framebuffer) {
        if (framebuffer !== undefined) {
            if (this.state.framebuffer !== framebuffer.buffer) {
                this.context.bindFramebuffer(GL.FRAMEBUFFER, framebuffer.buffer);
                this.state.framebuffer = framebuffer.buffer;
            }
            if (this.state.drawingWidth !== framebuffer.width || this.state.drawingHeight !== framebuffer.height) {
                this.context.viewport(0, 0, framebuffer.width, framebuffer.height);
                this.state = __assign(__assign({}, this.state), { drawingWidth: framebuffer.width, drawingHeight: framebuffer.height });
            }
        }
        else {
            if (this.state.framebuffer != null) {
                this.context.bindFramebuffer(GL.FRAMEBUFFER, null);
                this.state.framebuffer = null;
            }
            var width = this.canvas.width;
            var height = this.canvas.height;
            if (this.canvas instanceof HTMLCanvasElement) {
                width = this.canvas.clientWidth * window.devicePixelRatio;
                height = this.canvas.clientHeight * window.devicePixelRatio;
            }
            if (this.context.canvas.width !== width || this.context.canvas.height !== height) {
                this.context.canvas.width = width;
                this.context.canvas.height = height;
            }
            width = this.context.drawingBufferWidth;
            height = this.context.drawingBufferHeight;
            if (this.state.drawingWidth !== width || this.state.drawingHeight !== height) {
                this.context.viewport(0, 0, width, height);
                this.state = __assign(__assign({}, this.state), { drawingWidth: width, drawingHeight: height });
            }
        }
        /* tslint:disable-next-line:no-bitwise */
        this.context.clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT);
        if (this.vertices == null) {
            throw new Error('Uninitialized state: no vertex');
        }
        if (this.shaderProgram == null) {
            throw new Error('Uninitialized state: no program');
        }
        if (this.shaderAttributes == null) {
            throw new Error('Uninitialized state: no attributes');
        }
        this.context.bindBuffer(this.context.ARRAY_BUFFER, this.vertices.buffer);
        this.context.vertexAttribPointer(this.shaderAttributes.vertexPosition, this.vertices.itemSize, GL.FLOAT, false, 0, 0);
        if (params) {
            var texIndex = 1;
            for (var _i = 0, _a = Object.getOwnPropertyNames(params); _i < _a.length; _i++) {
                var p = _a[_i];
                var pInfo = p.split('_');
                if (pInfo.length !== 2) {
                    throw new Error("Invalid uniform ".concat(p, " in draw call, please specify [type]_[uniform name]"));
                }
                var pType = pInfo[0], pName = pInfo[1];
                var pValue = params[p];
                var uLocation = this.context.getUniformLocation(this.shaderProgram, pName);
                switch (pType) {
                    case 'bool':
                        if (uBool(pValue)) {
                            this.context.uniform1i(uLocation, pValue ? 1 : 0);
                        }
                        break;
                    case 'int':
                        if (uNum(pValue)) {
                            this.context.uniform1i(uLocation, pValue);
                        }
                        break;
                    case 'ivec2':
                        if (uNumA(pValue)) {
                            this.context.uniform2i(uLocation, pValue[0], pValue[1]);
                        }
                        break;
                    case 'ivec3':
                        if (uNumA(pValue)) {
                            this.context.uniform3i(uLocation, pValue[0], pValue[1], pValue[2]);
                        }
                        break;
                    case 'ivec4':
                        if (uNumA(pValue)) {
                            this.context.uniform4i(uLocation, pValue[0], pValue[1], pValue[2], pValue[3]);
                        }
                        break;
                    case 'float':
                        if (uNum(pValue)) {
                            this.context.uniform1f(uLocation, pValue);
                        }
                        break;
                    case 'vec2':
                        if (uNumA(pValue)) {
                            this.context.uniform2f(uLocation, pValue[0], pValue[1]);
                        }
                        break;
                    case 'vec3':
                        if (uNumA(pValue)) {
                            this.context.uniform3f(uLocation, pValue[0], pValue[1], pValue[2]);
                        }
                        break;
                    case 'vec4':
                        if (uNumA(pValue)) {
                            this.context.uniform4f(uLocation, pValue[0], pValue[1], pValue[2], pValue[3]);
                        }
                        break;
                    case 'int[]':
                        if (uNumA(pValue)) {
                            this.context.uniform1iv(uLocation, pValue);
                        }
                        break;
                    case 'ivec2[]':
                        if (uNumA(pValue)) {
                            this.context.uniform2iv(uLocation, pValue);
                        }
                        break;
                    case 'ivec3[]':
                        if (uNumA(pValue)) {
                            this.context.uniform3iv(uLocation, pValue);
                        }
                        break;
                    case 'ivec4[]':
                        if (uNumA(pValue)) {
                            this.context.uniform4iv(uLocation, pValue);
                        }
                        break;
                    case 'float[]':
                        if (uNumA(pValue)) {
                            this.context.uniform1fv(uLocation, pValue);
                        }
                        break;
                    case 'vec2[]':
                        if (uNumA(pValue)) {
                            this.context.uniform2fv(uLocation, pValue);
                        }
                        break;
                    case 'vec3[]':
                        if (uNumA(pValue)) {
                            this.context.uniform3fv(uLocation, pValue);
                        }
                        break;
                    case 'vec4[]':
                        if (uNumA(pValue)) {
                            this.context.uniform4fv(uLocation, pValue);
                        }
                        break;
                    case 'mat2':
                        if (uNumA(pValue)) {
                            this.context.uniformMatrix2fv(uLocation, false, pValue);
                        }
                        break;
                    case 'mat3':
                        if (uNumA(pValue)) {
                            this.context.uniformMatrix3fv(uLocation, false, pValue);
                        }
                        break;
                    case 'mat4':
                        if (uNumA(pValue)) {
                            this.context.uniformMatrix4fv(uLocation, false, pValue);
                        }
                        break;
                    case 'texture':
                        this.context.activeTexture(GL["TEXTURE".concat(texIndex)]);
                        this.context.bindTexture(GL.TEXTURE_2D, pValue);
                        this.context.uniform1i(uLocation, texIndex);
                        ++texIndex;
                        break;
                    default:
                        throw new Error("Unknown type ".concat(pType, " for uniform ").concat(p, " in draw call"));
                }
            }
        }
        this.context.drawArrays(this.drawMode, 0, this.vertices.numItems);
    };
    SimpleGL.prototype.copyCurrentBufferToTexture = function (tx) {
        this.context.activeTexture(GL.TEXTURE0);
        this.context.bindTexture(GL.TEXTURE_2D, tx);
        this.context
            .copyTexImage2D(GL.TEXTURE_2D, 0, GL.RGBA, 0, 0, this.context.drawingBufferWidth, this.context.drawingBufferHeight, 0);
    };
    return SimpleGL;
}());
exports.default = SimpleGL;
function uBool(x) {
    return typeof x === 'boolean';
}
function uNum(x) {
    return typeof x === 'number';
}
function uNumA(x) {
    return Array.isArray(x);
}
//# sourceMappingURL=simplegl.js.map
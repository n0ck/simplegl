"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var simplegl_1 = require("./simplegl");
var GL = WebGLRenderingContext;
var SimpleGLImage = /** @class */ (function (_super) {
    __extends(SimpleGLImage, _super);
    function SimpleGLImage(canvas) {
        var _this = _super.call(this, canvas) || this;
        _this.textures = [];
        _this.additionalShaderAttributes = {};
        _this.setVertices([-1, -1, 0, -1, 1, 0, 1, -1, 0, 1, 1, 0], 3);
        var data = new Float32Array([0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0]);
        var buffer = _this.context.createBuffer();
        _this.context.bindBuffer(GL.ARRAY_BUFFER, buffer);
        _this.context.bufferData(GL.ARRAY_BUFFER, data, GL.STATIC_DRAW);
        _this.textureCoordinates = {
            buffer: buffer,
            data: data,
            itemSize: 2,
            numItems: 4,
        };
        return _this;
    }
    SimpleGLImage.prototype.setProgram = function (shaderProgram) {
        _super.prototype.setProgram.call(this, shaderProgram);
        this.additionalShaderAttributes.textureCoordinate = this.context.getAttribLocation(shaderProgram, 'aTextureCoord');
        this.context.enableVertexAttribArray(this.additionalShaderAttributes.textureCoordinate);
        this.context.bindBuffer(GL.ARRAY_BUFFER, this.textureCoordinates.buffer);
        this.context.vertexAttribPointer(this.additionalShaderAttributes.textureCoordinate, this.textureCoordinates.itemSize, GL.FLOAT, false, 0, 0);
    };
    return SimpleGLImage;
}(simplegl_1.default));
exports.default = SimpleGLImage;
//# sourceMappingURL=simplegl-image.js.map
export default interface ISGLFrameBuffer {
    buffer: WebGLFramebuffer;
    texture: WebGLTexture;
    width: number;
    height: number;
}
import ISGLFrameBuffer from "./ISGLFrameBuffer";

export type FrameBuffer = ISGLFrameBuffer;

const GL = WebGLRenderingContext;

export interface SimpleGLOptions extends WebGLContextAttributes {
}

export default class SimpleGL {
  protected canvas: HTMLCanvasElement;
  protected context: WebGLRenderingContext;
  protected state:{
    framebuffer: WebGLFramebuffer | null;
    drawingWidth: number;
    drawingHeight: number;
  } = {
    drawingHeight: 0,
    drawingWidth: 0,
    framebuffer: null,
  }

  protected shaderProgram?: WebGLProgram;
  protected shaderAttributes?: {
    vertexPosition: number;
  };

  protected vertices?: {
    data: Float32Array;
    buffer: WebGLBuffer;
    itemSize: number;
    numItems: number;
  };

  protected drawMode: number;

  constructor(canvas: HTMLCanvasElement, options?: SimpleGLOptions) {
    this.canvas = canvas;
    this.context = canvas.getContext('webgl', options)!;

    this.drawMode = GL.TRIANGLE_STRIP;
  }

  public get drawWidth() { return this.context.drawingBufferWidth }
  public get drawHeight() { return this.context.drawingBufferHeight }
  public getCanvas():Readonly<HTMLCanvasElement> { return this.canvas }
  public getGLContext():Readonly<WebGLRenderingContext> { return this.context }

  public setProgram(shaderProgram: WebGLProgram) {
    this.shaderProgram = shaderProgram;
    this.context.useProgram(this.shaderProgram);

    this.shaderAttributes = {
      vertexPosition: this.context.getAttribLocation(this.shaderProgram, 'aVertexPosition'),
    };
    this.context.enableVertexAttribArray(this.shaderAttributes.vertexPosition);
  }

  public createProgram(vs: string, fs: string) {
    function getFragmentShader(context: WebGLRenderingContext, src: string) {
      const shader = context.createShader(GL.FRAGMENT_SHADER)!;
      context.shaderSource(shader, src);
      context.compileShader(shader);
      if (!context.getShaderParameter(shader, GL.COMPILE_STATUS)) {
        let err = context.getShaderInfoLog(shader);
        if( err ) {
          const match = /ERROR: (\d+):(\d+):/.exec(err);
          if(match){
            const [errIndex, char, line] = match;
            const errLine = src.split('\n')[parseInt(line, 10)-1].trim();
            const baseErr = err.split('\n');
            err = [
              baseErr[0].slice(errIndex.length).trim(),
              `l.${line}: ${errLine}`,
              `${new Array(`l.${line}: `.length+1+parseInt(char, 10)).join(' ')}^`,
              baseErr[1].slice(errIndex.length).trim()
            ]
            .concat(baseErr[2])
            .join('\n');
          }
        }
        throw new Error(err || 'unknown gl error');
      }
      return shader;
    }

    function getVertexShader(context: WebGLRenderingContext, src: string) {
      const shader = context.createShader(GL.VERTEX_SHADER)!;
      context.shaderSource(shader, src);
      context.compileShader(shader);
      if (!context.getShaderParameter(shader, GL.COMPILE_STATUS)) {
        let err = context.getShaderInfoLog(shader);
        if( err ) {
          const match = /ERROR: (\d+):(\d+):/.exec(err);
          if(match){
            const [errIndex, char, line] = match;
            const errLine = src.split('\n')[parseInt(line, 10)-1].trim();
            const baseErr = err.split('\n');
            err = [
              baseErr[0].slice(errIndex.length).trim(),
              `l.${line}: ${errLine}`,
              `${new Array(`l.${line}: `.length+1+parseInt(char, 10)).join(' ')}^`,
              baseErr[1].slice(errIndex.length).trim()
            ]
            .concat(baseErr[2])
            .join('\n');
          }
        }
        throw new Error(err || 'unknown gl error');
      }
      return shader;
    }

    const vertexShader = getVertexShader(this.context, vs);
    const fragmentShader = getFragmentShader(this.context, fs);

    const shaderProgram = this.context.createProgram();
    if (shaderProgram == null) {
      throw new Error('error compiling program');
    }
    this.context.attachShader(shaderProgram, vertexShader);
    this.context.attachShader(shaderProgram, fragmentShader);
    this.context.linkProgram(shaderProgram);
    if (!this.context.getProgramParameter(shaderProgram, GL.LINK_STATUS)) {
      throw new Error('Could not initialise shaders');
    }

    return shaderProgram;
  }

  public setVertices(vertices: number[], itemSize: number = 3) {
    const numItems = Math.floor(vertices.length / itemSize);

    const data = new Float32Array(vertices);
    const buffer = this.context.createBuffer();
    if (buffer == null) {
      throw new Error('error creating buffer');
    }
    this.context.bindBuffer(this.context.ARRAY_BUFFER, buffer);
    this.context.bufferData(this.context.ARRAY_BUFFER, data, GL.STATIC_DRAW);

    this.vertices = {
      buffer,
      data,
      itemSize,
      numItems,
    };
  }

  public createTexture(): WebGLTexture {
    const tex = this.context.createTexture()!;
    this.context.bindTexture(GL.TEXTURE_2D, tex);
    this.context.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR);
    this.context.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);
    this.context.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE);
    this.context.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE);
    return tex;
  }

  public updateTexture(tex: WebGLTexture, src: TexImageSource):void;
  public updateTexture(tex: WebGLTexture, width: number, height: number, data?: { format: number, type: number, pixels: ArrayBufferView }):void;
  public updateTexture(tex: WebGLTexture, srcOrWidth: TexImageSource | number, height?: number, data?: { format: number, type: number, pixels: ArrayBufferView }) {
    if (!tex) {
      return;
    }

    this.context.bindTexture(GL.TEXTURE_2D, tex);
    this.context.pixelStorei(GL.UNPACK_FLIP_Y_WEBGL, 1);

    if(typeof srcOrWidth === 'number'){
      const width = srcOrWidth;
      this.context.texImage2D( GL.TEXTURE_2D, 0, GL.RGBA, width, height!, 0, data?.format ?? GL.RGBA, data?.type ?? GL.UNSIGNED_BYTE, data?.pixels ?? null );
    } else {
      const src = srcOrWidth;
      if ((src instanceof HTMLVideoElement ? src.videoHeight : src.height) < 1) {
        throw new Error('invalid source provided to updateTexture');
      }
      this.context.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, GL.RGBA, GL.UNSIGNED_BYTE, src);
    }
  }

  public createFramebuffer(width?: number, height?:number):FrameBuffer {
    const buf = this.context.createFramebuffer()!;
    this.context.bindFramebuffer(GL.FRAMEBUFFER, buf);
    const tex = this.createTexture();
    const w = width || this.canvas.width;
    const h = height || this.canvas.height;
    this.context.texImage2D( GL.TEXTURE_2D, 0, GL.RGBA, w, h, 0, GL.RGBA, GL.UNSIGNED_BYTE, null );
    this.context.framebufferTexture2D(GL.FRAMEBUFFER, GL.COLOR_ATTACHMENT0, GL.TEXTURE_2D, tex, 0 );
    // clean
    this.context.bindFramebuffer( GL.FRAMEBUFFER, this.state.framebuffer );
    return {
      buffer: buf,
      height: h,
      texture: tex,
      width: w,
    }
  }

  public draw(
    params?: { [id: string]: boolean | number | number[] | WebGLTexture },
    framebuffer?: FrameBuffer
  ) {

    if(framebuffer !== undefined) {
      if(this.state.framebuffer !== framebuffer.buffer){
        this.context.bindFramebuffer(GL.FRAMEBUFFER, framebuffer.buffer);
        this.state.framebuffer = framebuffer.buffer;
      }
      if(this.state.drawingWidth !== framebuffer.width || this.state.drawingHeight !== framebuffer.height){
        this.context.viewport(0, 0, framebuffer.width, framebuffer.height);
        this.state = {...this.state, drawingWidth: framebuffer.width, drawingHeight: framebuffer.height};
      }
    } else {
      if(this.state.framebuffer != null){
        this.context.bindFramebuffer(GL.FRAMEBUFFER, null);
        this.state.framebuffer = null;
      }

      let width = this.canvas.width;
      let height = this.canvas.height;
      if( this.canvas instanceof HTMLCanvasElement ) {
        width = this.canvas.clientWidth * window.devicePixelRatio;
        height = this.canvas.clientHeight * window.devicePixelRatio;
      }

      if (this.context.canvas.width !== width || this.context.canvas.height !== height) {
        this.context.canvas.width = width;
        this.context.canvas.height = height;
      }
      width = this.context.drawingBufferWidth;
      height = this.context.drawingBufferHeight;
      if(this.state.drawingWidth !== width || this.state.drawingHeight !== height){
        this.context.viewport(0, 0, width, height);
        this.state = {...this.state, drawingWidth: width, drawingHeight: height};
      }
    }

    /* tslint:disable-next-line:no-bitwise */
    this.context.clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT);

    if (this.vertices == null) {
      throw new Error('Uninitialized state: no vertex');
    }
    if (this.shaderProgram == null) {
      throw new Error('Uninitialized state: no program');
    }
    if (this.shaderAttributes == null) {
      throw new Error('Uninitialized state: no attributes');
    }

    this.context.bindBuffer(this.context.ARRAY_BUFFER, this.vertices.buffer);
    this.context.vertexAttribPointer(
      this.shaderAttributes.vertexPosition,
      this.vertices.itemSize,
      GL.FLOAT,
      false,
      0,
      0,
    );

    if (params) {
      let texIndex = 1;

      for(const p of Object.getOwnPropertyNames(params)) {
        const pInfo = p.split('_');
        if (pInfo.length !== 2) {
          throw new Error(`Invalid uniform ${p} in draw call, please specify [type]_[uniform name]`);
        }
        const [pType, pName] = pInfo;
        const pValue = params[p];

        const uLocation = this.context.getUniformLocation(this.shaderProgram, pName);
        
        switch (pType) {
          case 'bool':
            if (uBool(pValue)) {
              this.context.uniform1i(uLocation, pValue ? 1 : 0);
            }
            break;
          case 'int':
            if (uNum(pValue)) {
              this.context.uniform1i(uLocation, pValue);
            }
            break;
          case 'ivec2':
            if (uNumA(pValue)) {
              this.context.uniform2i(uLocation, pValue[0], pValue[1]);
            }
            break;
          case 'ivec3':
            if (uNumA(pValue)) {
              this.context.uniform3i(uLocation, pValue[0], pValue[1], pValue[2]);
            }
            break;
          case 'ivec4':
            if (uNumA(pValue)) {
              this.context.uniform4i(uLocation, pValue[0], pValue[1], pValue[2], pValue[3]);
            }
            break;
          case 'float':
            if (uNum(pValue)) {
              this.context.uniform1f(uLocation, pValue);
            }
            break;
          case 'vec2':
            if (uNumA(pValue)) {
              this.context.uniform2f(uLocation, pValue[0], pValue[1]);
            }
            break;
          case 'vec3':
            if (uNumA(pValue)) {
              this.context.uniform3f(uLocation, pValue[0], pValue[1], pValue[2]);
            }
            break;
          case 'vec4':
            if (uNumA(pValue)) {
              this.context.uniform4f(uLocation, pValue[0], pValue[1], pValue[2], pValue[3]);
            }
            break;
          case 'int[]':
            if (uNumA(pValue)) {
              this.context.uniform1iv(uLocation, pValue);
            }
            break;
          case 'ivec2[]':
            if (uNumA(pValue)) {
              this.context.uniform2iv(uLocation, pValue);
            }
            break;
          case 'ivec3[]':
            if (uNumA(pValue)) {
              this.context.uniform3iv(uLocation, pValue);
            }
            break;
          case 'ivec4[]':
            if (uNumA(pValue)) {
              this.context.uniform4iv(uLocation, pValue);
            }
            break;
          case 'float[]':
            if (uNumA(pValue)) {
              this.context.uniform1fv(uLocation, pValue);
            }
            break;
          case 'vec2[]':
            if (uNumA(pValue)) {
              this.context.uniform2fv(uLocation, pValue);
            }
            break;
          case 'vec3[]':
            if (uNumA(pValue)) {
              this.context.uniform3fv(uLocation, pValue);
            }
            break;
          case 'vec4[]':
            if (uNumA(pValue)) {
              this.context.uniform4fv(uLocation, pValue);
            }
            break;
          case 'mat2':
            if (uNumA(pValue)) {
              this.context.uniformMatrix2fv(uLocation, false, pValue);
            }
            break;
          case 'mat3':
            if (uNumA(pValue)) {
              this.context.uniformMatrix3fv(uLocation, false, pValue);
            }
            break;
          case 'mat4':
            if (uNumA(pValue)) {
              this.context.uniformMatrix4fv(uLocation, false, pValue);
            }
            break;

          case 'texture':
            this.context.activeTexture((GL as any)[`TEXTURE${texIndex}`] as number);
            this.context.bindTexture(GL.TEXTURE_2D, pValue);
            this.context.uniform1i(uLocation, texIndex);
            ++texIndex;
            break;

          default:
            throw new Error(`Unknown type ${pType} for uniform ${p} in draw call`);
        }
      }
    }

    this.context.drawArrays(this.drawMode, 0, this.vertices.numItems);
  }

  public copyCurrentBufferToTexture(tx: WebGLTexture) {

    this.context.activeTexture(GL.TEXTURE0);
    this.context.bindTexture(GL.TEXTURE_2D, tx);

    this.context
      .copyTexImage2D(
        GL.TEXTURE_2D,
        0,
        GL.RGBA,
        0,
        0,
        this.context.drawingBufferWidth,
        this.context.drawingBufferHeight,
        0,
      );
  }

}

function uBool(x: any): x is boolean {
  return typeof x === 'boolean';
}

function uNum(x: any): x is number {
  return typeof x === 'number';
}

function uNumA(x: any): x is number[] {
  return Array.isArray(x);
}

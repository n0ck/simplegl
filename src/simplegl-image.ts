import SimpleGL from './simplegl';

const GL = WebGLRenderingContext;

export default class SimpleGLImage extends SimpleGL {
  protected textures: WebGLTexture[] = [];
  protected additionalShaderAttributes: {
    textureCoordinate?: number;
  } = {};

  protected textureCoordinates: {
    data: Float32Array;
    buffer: WebGLBuffer;
    itemSize: number;
    numItems: number;
  };

  constructor(canvas: HTMLCanvasElement) {
    super(canvas);

    this.setVertices([-1, -1, 0, -1, 1, 0, 1, -1, 0, 1, 1, 0], 3);

    const data = new Float32Array([0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0]);
    const buffer = this.context.createBuffer()!;
    this.context.bindBuffer(GL.ARRAY_BUFFER, buffer);
    this.context.bufferData(GL.ARRAY_BUFFER, data, GL.STATIC_DRAW);
    this.textureCoordinates = {
      buffer,
      data,
      itemSize: 2,
      numItems: 4,
    };
  }

  public setProgram(shaderProgram: WebGLProgram) {
    super.setProgram(shaderProgram);

    this.additionalShaderAttributes.textureCoordinate = this.context.getAttribLocation(shaderProgram, 'aTextureCoord');
    this.context.enableVertexAttribArray(this.additionalShaderAttributes.textureCoordinate);
    this.context.bindBuffer(GL.ARRAY_BUFFER, this.textureCoordinates.buffer);
    this.context.vertexAttribPointer(
      this.additionalShaderAttributes.textureCoordinate,
      this.textureCoordinates.itemSize,
      GL.FLOAT,
      false,
      0,
      0,
    );
  }
}
